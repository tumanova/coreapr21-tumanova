package com.epam.learn.l4;

public class Main {
    //break-прерывание
    //continue-запуск следующей итерации цикла

    public static void main(String[] args) {
        int i = 0;

        while (true) {
            i++;

            if (i == 50) {
                continue;
            }

            System.out.println("current i is: " + i);

            if (i == 100){
                break;
            }
        }
    }
}
