package com.epam.learn.l2;

public class StaticInitial {
    private static int count = 0;

    static {
        count = 5;
    }

    public static void main(String[] args) {
        System.out.println(count);
    }
}
