package com.epam.learn.l2;

public class Logic {
    // &&, ||, >, < , ==, !=, <=, >=
    private boolean paramA = true;
    private boolean paramB = false;
    private boolean paramC = true;

    public static void main(String[] args) {
        Logic logic = new Logic();
        System.out.println(logic.checkResultFirst(logic.paramA, logic.paramB, logic.paramC));
        System.out.println(logic.checkResult2(logic.paramA, logic.paramB));
        System.out.println(logic.checkResult3(logic.paramC, logic.paramB));
        System.out.println(logic.checkResult4(logic.paramA, logic.paramB, logic.paramC));
    }

    // 1) A && B || C
    private boolean checkResultFirst(boolean a, boolean b, boolean c){
        return a && b || c;
    }

    //2) A && B
    private boolean checkResult2(boolean a, boolean b){
        return a && b;
    }

    //3) C || B
    private boolean checkResult3(boolean c, boolean b){
        return c || b;
    }

    //4) A || B && C
    private boolean checkResult4(boolean a, boolean b, boolean c){
        return a || b && c;
    }
}
