package com.epam.learn.l2;

public class Main2 {
    public static void main(String[] args) {
        Main2 main = new Main2();
        System.out.println(main.isEven(2));
        System.out.println(main.isOdd(5));
    }
    private boolean isEven (int param) {
        return param % 2 == 0;
    }

    private boolean isOdd (int param) {
        return param % 2 != 0;
    }
}
