package com.epam.learn.l1;

public class PrimitiveExample {
    //Целые:
    byte someByte;
    //-128 127 (8 бит)
    short someShort;
    //-2^15 2^15-1 (16 бит)
    int number = 1;
    //-2^31 2^31-1 (32 бита)
    long bigNumber;
    //-2^63 2^63-1 (64 бита)

    // дробные (ieee 754 формат)
    float nFloat;
    //(32 бита)
    double nDouble;
    //(64 бита)

    //символьный
    char someChar;
    //(16 бит)

    //логический
    boolean isTrue;
    //не указываем размер тк размер зависит от реализации конкретной машины
}
